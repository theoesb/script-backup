import argparse
import datetime
import os
import zipfile


def main():
    parser = argparse.ArgumentParser(prog='backup', description='script for backup folder')
    parser.add_argument('source', type=str, help="Absolut path of directory source")
    parser.add_argument('destination', type=str, help="Absolut path of directory destination")
    args = parser.parse_args()

    source_dir = args.source
    dest_dir = args.destination

    if not os.path.exists(source_dir):
        print("Le fichier source n'éxiste pas")

    if not os.path.exists(dest_dir):
        print("le fichier destination n'éxiste pas")

    backup_directory(source_dir, dest_dir)


def backup_directory(source_dir, dest_dir):
    timestamp = datetime.datetime.now().strftime('%Y_%m_%d_%Hh%M')
    backup_filename = f"backup_{timestamp}.zip"
    backup_path = os.path.join(dest_dir, backup_filename)

    with zipfile.ZipFile(backup_path, 'w') as backup:
        for root, _, files in os.walk(source_dir):
            for file in files:
                file_path = os.path.join(root, file)
                rel_path = os.path.relpath(file_path, source_dir)
                backup.write(file_path, rel_path)

                # Ajouter les répertoires vides à l'archive
                for dir in os.walk(source_dir):
                    dir_path = dir[0]
                    rel_path = os.path.relpath(dir_path, source_dir)
                    if not os.listdir(dir_path):
                        # Le répertoire est vide, donc nous l'ajoutons à l'archive
                        zinfo = zipfile.ZipInfo(rel_path + os.sep)
                        backup.writestr(zinfo, '')

    print(f"Sauvegarde terminée. Fichier de sauvegarde créé : {backup_path}")


if __name__ == "__main__":
    main()
